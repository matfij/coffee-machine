/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: KeyReader
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/KeyReader.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/KeyReader.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class KeyReader 
public class KeyReader implements RiJStateConcept {
    
    public Reactive reactive;		//## ignore 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int action_1=1;
    public static final int action_0=2;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  KeyReader(RiJThread p_thread) {
        reactive = new Reactive(p_thread);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case action_0:
                {
                    res = action_0_takeEvent(id);
                }
                break;
                case action_1:
                {
                    res = action_1_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void action_0Exit() {
        }
        
        //## statechart_method 
        public void action_1Exit() {
        }
        
        //## statechart_method 
        public int action_1TakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            action_1_exit();
            action_1_entDef();
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int action_1_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = action_1TakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int action_0TakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            action_0_exit();
            action_1_entDef();
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void action_0_entDef() {
            action_0_enter();
        }
        
        //## statechart_method 
        public void rootState_enter() {
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void action_1Enter() {
            //#[ state action_1.(Entry) 
            char cmd = 0;
            try
            {
            while (Character.isLetterOrDigit(cmd) == false)
            cmd = (char)System.in.read();
            }
            catch (java.io.IOException e)
            {
            System.err.println("Exception while reading from console: " + e);
            }
            if (itsDisplay != null)
            itsDisplay.gen(new evKeyPress(cmd));
            //#]
        }
        
        //## statechart_method 
        public int action_0_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = action_0TakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void action_0Enter() {
            //#[ state action_0.(Entry) 
            System.out.println("Enter command:");
            //#]
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            action_0_entDef();
        }
        
        //## statechart_method 
        public void action_1_exit() {
            popNullConfig();
            action_1Exit();
        }
        
        //## statechart_method 
        public void action_0_exit() {
            popNullConfig();
            action_0Exit();
        }
        
        //## statechart_method 
        public void action_1_entDef() {
            action_1_enter();
        }
        
        //## statechart_method 
        public void action_0_enter() {
            pushNullConfig();
            rootState_subState = action_0;
            rootState_active = action_0;
            action_0Enter();
        }
        
        //## statechart_method 
        public void action_1_enter() {
            pushNullConfig();
            rootState_subState = action_1;
            rootState_active = action_1;
            action_1Enter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
    }
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/KeyReader.java
*********************************************************************/

