/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evKeyPressed
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evKeyPressed.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evKeyPressed.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evKeyPressed(char) 
public class evKeyPressed extends RiJEvent {
    
    public static final int evKeyPressed_coffeemachine_telelogic_com_id = 29219;		//## ignore 
    
    public char key;
    
    // Constructors
    
    public  evKeyPressed(char p_key) {
        lId = evKeyPressed_coffeemachine_telelogic_com_id;
        key = p_key;
    }
    
    public boolean isTypeOf(long id) {
        return (evKeyPressed_coffeemachine_telelogic_com_id==id);
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evKeyPressed.java
*********************************************************************/

