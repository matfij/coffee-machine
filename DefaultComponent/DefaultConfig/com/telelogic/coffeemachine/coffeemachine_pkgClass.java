/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: com.telelogic.coffeemachine
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/coffeemachine_pkgClass.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.telelogic.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/coffeemachine_pkgClass.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## ignore 
public class coffeemachine_pkgClass {
    
    public static object_0 object_0;		//## classInstance object_0 
    
    
    // Constructors
    
    //## auto_generated 
    public  coffeemachine_pkgClass(RiJThread p_thread) {
        initRelations();
    }
    
    //## auto_generated 
    protected void finalize() throws Throwable {
        
        super.finalize();
    }
    
    //## auto_generated 
    protected void initRelations() {
        object_0 = new object_0();
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/coffeemachine_pkgClass.java
*********************************************************************/

