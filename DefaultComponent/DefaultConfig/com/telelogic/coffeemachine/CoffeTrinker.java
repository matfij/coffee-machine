/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: CoffeTrinker
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/CoffeTrinker.java
*********************************************************************/

package com.telelogic.coffeemachine;


//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/CoffeTrinker.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


/**
[[ * @author $Author]]
[[ * @version $Version]]
[[ * @see $See]]
[[ * @since $Since]]
*/
//## actor CoffeTrinker 
public class CoffeTrinker {
    
    
    // Constructors
    
    //## auto_generated 
    public  CoffeTrinker() {
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/CoffeTrinker.java
*********************************************************************/

