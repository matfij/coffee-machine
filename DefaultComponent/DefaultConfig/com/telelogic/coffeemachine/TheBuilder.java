/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TheBuilder
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/TheBuilder.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/TheBuilder.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class TheBuilder 
public class TheBuilder implements RiJStateConcept {
    
    public Reactive reactive;		//## ignore 
    
    protected Coffeemachine itsCoffeemachine;		//## classInstance itsCoffeemachine 
    
    protected Display itsDisplay;		//## classInstance itsDisplay 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  TheBuilder(RiJThread p_thread) {
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
    }
    
    //## auto_generated 
    public Coffeemachine getItsCoffeemachine() {
        return itsCoffeemachine;
    }
    
    //## auto_generated 
    public Coffeemachine newItsCoffeemachine(RiJThread p_thread) {
        itsCoffeemachine = new Coffeemachine(p_thread);
        return itsCoffeemachine;
    }
    
    //## auto_generated 
    public void deleteItsCoffeemachine() {
        itsCoffeemachine=null;
    }
    
    //## auto_generated 
    public Display getItsDisplay() {
        return itsDisplay;
    }
    
    //## auto_generated 
    public Display newItsDisplay(RiJThread p_thread) {
        itsDisplay = new Display(p_thread);
        return itsDisplay;
    }
    
    //## auto_generated 
    public void deleteItsDisplay() {
        itsDisplay=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsCoffeemachine = newItsCoffeemachine(p_thread);
        itsDisplay = newItsDisplay(p_thread);
        itsDisplay.setItsCoffeemachine(itsCoffeemachine);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsCoffeemachine.startBehavior();
        done &= itsDisplay.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
        }
        
    }
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/TheBuilder.java
*********************************************************************/

