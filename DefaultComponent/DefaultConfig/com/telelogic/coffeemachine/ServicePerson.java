/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: ServicePerson
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/ServicePerson.java
*********************************************************************/

package com.telelogic.coffeemachine;


//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/ServicePerson.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


/**
[[ * @author $Author]]
[[ * @version $Version]]
[[ * @see $See]]
[[ * @since $Since]]
*/
//## actor ServicePerson 
public class ServicePerson {
    
    
    // Constructors
    
    //## auto_generated 
    public  ServicePerson() {
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/ServicePerson.java
*********************************************************************/

