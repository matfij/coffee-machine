/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evStart
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evStart.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evStart.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evStart() 
public class evStart extends RiJEvent {
    
    public static final int evStart_coffeemachine_telelogic_com_id = 29216;		//## ignore 
    
    
    // Constructors
    
    public  evStart() {
        lId = evStart_coffeemachine_telelogic_com_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evStart_coffeemachine_telelogic_com_id==id);
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evStart.java
*********************************************************************/

