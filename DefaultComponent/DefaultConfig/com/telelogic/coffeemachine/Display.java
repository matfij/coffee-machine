/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Display
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/Display.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/Display.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class Display 
public class Display implements RiJStateConcept {
    
    public Reactive reactive;		//## ignore 
    
    protected Coffeemachine itsCoffeemachine;		//## link itsCoffeemachine 
    
    protected KeyReader itsKeyReader;		//## classInstance itsKeyReader 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int WaitForInput=1;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Display(RiJThread p_thread) {
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
    }
    
    /**
     * @param key
    */
    //## operation processKey(char) 
    public void processKey(char key) {
        //#[ operation processKey(char) 
        //#]
    }
    
    //## auto_generated 
    public Coffeemachine getItsCoffeemachine() {
        return itsCoffeemachine;
    }
    
    //## auto_generated 
    public void __setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        itsCoffeemachine = p_Coffeemachine;
    }
    
    //## auto_generated 
    public void _setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        if(itsCoffeemachine != null)
            {
                itsCoffeemachine.__setItsDisplay(null);
            }
        __setItsCoffeemachine(p_Coffeemachine);
    }
    
    //## auto_generated 
    public void setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        if(p_Coffeemachine != null)
            {
                p_Coffeemachine._setItsDisplay(this);
            }
        _setItsCoffeemachine(p_Coffeemachine);
    }
    
    //## auto_generated 
    public void _clearItsCoffeemachine() {
        itsCoffeemachine = null;
    }
    
    //## auto_generated 
    public KeyReader getItsKeyReader() {
        return itsKeyReader;
    }
    
    //## auto_generated 
    public KeyReader newItsKeyReader(RiJThread p_thread) {
        itsKeyReader = new KeyReader(p_thread);
        return itsKeyReader;
    }
    
    //## auto_generated 
    public void deleteItsKeyReader() {
        itsKeyReader=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsKeyReader = newItsKeyReader(p_thread);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsKeyReader.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(rootState_active == WaitForInput)
                {
                    res = WaitForInput_takeEvent(id);
                }
            return res;
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void WaitForInput_exit() {
            WaitForInputExit();
        }
        
        //## statechart_method 
        public int WaitForInput_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evKeyPressed.evKeyPressed_coffeemachine_telelogic_com_id))
                {
                    res = WaitForInputTakeevKeyPressed();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void WaitForInput_entDef() {
            WaitForInput_enter();
        }
        
        //## statechart_method 
        public int WaitForInputTakeevKeyPressed() {
            evKeyPressed params = (evKeyPressed) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            //## transition 2 
            if(params.key == 's')
                {
                    WaitForInput_exit();
                    //#[ transition 2 
                    itsDishwasher.gen(new evStart());
                    //#]
                    WaitForInput_entDef();
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            else
                {
                    //## transition 3 
                    if(params.key == 'e')
                        {
                            WaitForInput_exit();
                            //#[ transition 3 
                            itsDishwasher.gen(new evEmpty());
                            //#]
                            WaitForInput_entDef();
                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                        }
                    else
                        {
                            //## transition 4 
                            if(params.key == 'f')
                                {
                                    WaitForInput_exit();
                                    //#[ transition 4 
                                    itsDishwasher.gen(new evRefilled());
                                    //#]
                                    WaitForInput_entDef();
                                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                                }
                            else
                                {
                                    //## transition 5 
                                    if(params.key == 'z')
                                        {
                                            WaitForInput_exit();
                                            //#[ transition 5 
                                            System.exit(1);
                                            //#]
                                            WaitForInput_entDef();
                                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                                        }
                                }
                        }
                }
            return res;
        }
        
        //## statechart_method 
        public void WaitForInputExit() {
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void rootState_enter() {
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            WaitForInput_entDef();
        }
        
        //## statechart_method 
        public void WaitForInput_enter() {
            rootState_subState = WaitForInput;
            rootState_active = WaitForInput;
            WaitForInputEnter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void WaitForInputEnter() {
        }
        
    }
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/Display.java
*********************************************************************/

