/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Coffeemachine
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/Coffeemachine.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/Coffeemachine.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class Coffeemachine 
public class Coffeemachine implements RiJStateConcept {
    
    public Reactive reactive;		//## ignore 
    
    protected int boilTime;		//## attribute boilTime 
    
    protected int grindTime;		//## attribute grindTime 
    
    protected int mixTime;		//## attribute mixTime 
    
    protected Display itsDisplay;		//## link itsDisplay 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int Waiting=1;
    public static final int Running=2;
    public static final int Mixing=3;
    public static final int Grinding=4;
    public static final int Boiling=5;
    public static final int Refilling=6;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    protected int Running_subState;		//## ignore 
    
    protected int Running_lastState;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Mixing_id = 1;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Grinding_id = 2;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Boiling_id = 3;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Coffeemachine(RiJThread p_thread) {
        reactive = new Reactive(p_thread);
    }
    
    //## operation setup() 
    public void setup() {
        //#[ operation setup() 
        boilTime = 2000;
        grindTime = 2000;
        mixTime = 2000;
        //#]
    }
    
    //## auto_generated 
    public int getBoilTime() {
        return boilTime;
    }
    
    //## auto_generated 
    public void setBoilTime(int p_boilTime) {
        boilTime = p_boilTime;
    }
    
    //## auto_generated 
    public int getGrindTime() {
        return grindTime;
    }
    
    //## auto_generated 
    public void setGrindTime(int p_grindTime) {
        grindTime = p_grindTime;
    }
    
    //## auto_generated 
    public int getMixTime() {
        return mixTime;
    }
    
    //## auto_generated 
    public void setMixTime(int p_mixTime) {
        mixTime = p_mixTime;
    }
    
    //## auto_generated 
    public Display getItsDisplay() {
        return itsDisplay;
    }
    
    //## auto_generated 
    public void __setItsDisplay(Display p_Display) {
        itsDisplay = p_Display;
    }
    
    //## auto_generated 
    public void _setItsDisplay(Display p_Display) {
        if(itsDisplay != null)
            {
                itsDisplay.__setItsCoffeemachine(null);
            }
        __setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void setItsDisplay(Display p_Display) {
        if(p_Display != null)
            {
                p_Display._setItsCoffeemachine(this);
            }
        _setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void _clearItsDisplay() {
        itsDisplay = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(Running_subState == state)
                {
                    return true;
                }
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case Grinding:
                {
                    res = Grinding_takeEvent(id);
                }
                break;
                case Boiling:
                {
                    res = Boiling_takeEvent(id);
                }
                break;
                case Mixing:
                {
                    res = Mixing_takeEvent(id);
                }
                break;
                case Waiting:
                {
                    res = Waiting_takeEvent(id);
                }
                break;
                case Refilling:
                {
                    res = Refilling_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
            Running_subState = RiJNonState;
            Running_lastState = RiJNonState;
        }
        
        //## statechart_method 
        public void Refilling_enter() {
            rootState_subState = Refilling;
            rootState_active = Refilling;
            RefillingEnter();
        }
        
        //## statechart_method 
        public int BoilingTakeevEmpty() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            Running_exit();
            Refilling_entDef();
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void GrindingEnter() {
            //#[ state Running.Grinding.(Entry) 
                          System.out.println("Grinding...");
            //#]
            itsRiJThread.schedTimeout(grindTime, Coffeemachine_Timeout_Grinding_id, this, null);
        }
        
        //## statechart_method 
        public int Running_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int MixingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Mixing_id)
                {
                    Running_exit();
                    Waiting_entDef();
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void RefillingEnter() {
            //#[ state Refilling.(Entry) 
              System.out.println("need more beans!");
            //#]
        }
        
        //## statechart_method 
        public int Grinding_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = GrindingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void Waiting_enter() {
            rootState_subState = Waiting;
            rootState_active = Waiting;
            WaitingEnter();
        }
        
        //## statechart_method 
        public void Grinding_exit() {
            GrindingExit();
        }
        
        //## statechart_method 
        public void RunningExit() {
        }
        
        //## statechart_method 
        public void Running_entDef() {
            Running_enter();
            
            Grinding_entDef();
        }
        
        //## statechart_method 
        public void RunningEnter() {
        }
        
        //## statechart_method 
        public void Refilling_exit() {
            RefillingExit();
        }
        
        //## statechart_method 
        public int Boiling_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = BoilingTakeRiJTimeout();
                }
            else if(event.isTypeOf(evEmpty.evEmpty_coffeemachine_telelogic_com_id))
                {
                    res = BoilingTakeevEmpty();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void Running_entShallowHist() {
            if(Running_lastState != RiJNonState)
                {
                    Running_subState = Running_lastState;
                    switch (Running_subState) {
                        case Grinding:
                        {
                            Grinding_enter();
                        }
                        break;
                        case Boiling:
                        {
                            Boiling_enter();
                        }
                        break;
                        case Mixing:
                        {
                            Mixing_enter();
                        }
                        break;
                        default:
                            break;
                    }
                }
        }
        
        //## statechart_method 
        public void Waiting_entDef() {
            Waiting_enter();
        }
        
        //## statechart_method 
        public void Grinding_enter() {
            Running_subState = Grinding;
            rootState_active = Grinding;
            GrindingEnter();
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void Mixing_entDef() {
            Mixing_enter();
        }
        
        //## statechart_method 
        public void Running_exit() {
            Running_lastState = Running_subState;
            switch (Running_subState) {
                case Grinding:
                {
                    Grinding_exit();
                    Running_lastState = Grinding;
                }
                break;
                case Boiling:
                {
                    Boiling_exit();
                    Running_lastState = Boiling;
                }
                break;
                case Mixing:
                {
                    Mixing_exit();
                    Running_lastState = Mixing;
                }
                break;
                default:
                    break;
            }
            Running_subState = RiJNonState;
            RunningExit();
        }
        
        //## statechart_method 
        public int Refilling_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evRefilled.evRefilled_coffeemachine_telelogic_com_id))
                {
                    res = RefillingTakeevRefilled();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int RefillingTakeevRefilled() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            Refilling_exit();
            Running_enter();
            Running_entShallowHist();
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void WaitingEnter() {
        }
        
        //## statechart_method 
        public void rootState_enter() {
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public int BoilingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Boiling_id)
                {
                    Boiling_exit();
                    Mixing_entDef();
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void Mixing_enter() {
            Running_subState = Mixing;
            rootState_active = Mixing;
            MixingEnter();
        }
        
        //## statechart_method 
        public void MixingEnter() {
            //#[ state Running.Mixing.(Entry) 
                      System.out.println("Mixing");
            //#]
            itsRiJThread.schedTimeout(mixTime, Coffeemachine_Timeout_Mixing_id, this, null);
        }
        
        //## statechart_method 
        public int Waiting_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evStart.evStart_coffeemachine_telelogic_com_id))
                {
                    res = WaitingTakeevStart();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void BoilingEnter() {
            //#[ state Running.Boiling.(Entry) 
                          System.out.println("Boiling...");
            //#]
            itsRiJThread.schedTimeout(boilTime, Coffeemachine_Timeout_Boiling_id, this, null);
        }
        
        //## statechart_method 
        public int Mixing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = MixingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void Boiling_entDef() {
            Boiling_enter();
        }
        
        //## statechart_method 
        public void MixingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Mixing_id, this);
        }
        
        //## statechart_method 
        public int WaitingTakeevStart() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            Waiting_exit();
            //#[ transition 4 
            setup();
            //#]
            Running_entDef();
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Waiting_exit() {
            WaitingExit();
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            Waiting_entDef();
        }
        
        //## statechart_method 
        public void Boiling_enter() {
            Running_subState = Boiling;
            rootState_active = Boiling;
            BoilingEnter();
        }
        
        //## statechart_method 
        public void Mixing_exit() {
            MixingExit();
        }
        
        //## statechart_method 
        public void Boiling_exit() {
            BoilingExit();
        }
        
        //## statechart_method 
        public void GrindingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Grinding_id, this);
        }
        
        //## statechart_method 
        public void Grinding_entDef() {
            Grinding_enter();
        }
        
        //## statechart_method 
        public void Running_enter() {
            rootState_subState = Running;
            RunningEnter();
        }
        
        //## statechart_method 
        public int GrindingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Grinding_id)
                {
                    Grinding_exit();
                    Boiling_entDef();
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void WaitingExit() {
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void RefillingExit() {
            //#[ state Refilling.(Exit) 
                 System.out.println("beans refilled!");
            //#]
        }
        
        //## statechart_method 
        public void Refilling_entDef() {
            Refilling_enter();
        }
        
        //## statechart_method 
        public void BoilingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Boiling_id, this);
        }
        
    }
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/Coffeemachine.java
*********************************************************************/

