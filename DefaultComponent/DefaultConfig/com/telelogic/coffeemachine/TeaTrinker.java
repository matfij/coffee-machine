/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TeaTrinker
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/TeaTrinker.java
*********************************************************************/

package com.telelogic.coffeemachine;


//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/TeaTrinker.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


/**
[[ * @author $Author]]
[[ * @version $Version]]
[[ * @see $See]]
[[ * @since $Since]]
*/
//## actor TeaTrinker 
public class TeaTrinker {
    
    
    // Constructors
    
    //## auto_generated 
    public  TeaTrinker() {
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/TeaTrinker.java
*********************************************************************/

