/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evRefilled
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evRefilled.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evRefilled.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evRefilled() 
public class evRefilled extends RiJEvent {
    
    public static final int evRefilled_coffeemachine_telelogic_com_id = 29217;		//## ignore 
    
    
    // Constructors
    
    public  evRefilled() {
        lId = evRefilled_coffeemachine_telelogic_com_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evRefilled_coffeemachine_telelogic_com_id==id);
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evRefilled.java
*********************************************************************/

