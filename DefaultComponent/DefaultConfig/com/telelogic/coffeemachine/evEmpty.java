/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evEmpty
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evEmpty.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evEmpty.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evEmpty() 
public class evEmpty extends RiJEvent {
    
    public static final int evEmpty_coffeemachine_telelogic_com_id = 29218;		//## ignore 
    
    
    // Constructors
    
    public  evEmpty() {
        lId = evEmpty_coffeemachine_telelogic_com_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evEmpty_coffeemachine_telelogic_com_id==id);
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/com/telelogic/coffeemachine/evEmpty.java
*********************************************************************/

