/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: Host
//!	Generated Date	: Fri, 19, Apr 2019 
	File Path	: EXE/Host/MainEXE.java
*********************************************************************/


//## auto_generated
import com.telelogic.coffeemachine.*;
//## auto_generated
import com.telelogic.coffeemachine.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.*;

//----------------------------------------------------------------------------
// MainEXE.java                                                                  
//----------------------------------------------------------------------------


//## ignore 
public class MainEXE {
    
    //#[ ignore
    // link with events in order to register them in the animation browser
    static {
      // Setting Animation Default Port 
      AnimTcpIpConnection.setDefaultPort(6423);
      // Registering Events 
      try {
        
            Class.forName("com.telelogic.coffeemachine.evEmpty");
            Class.forName("com.telelogic.coffeemachine.evKeyPressed");
            Class.forName("com.telelogic.coffeemachine.evRefilled");
            Class.forName("com.telelogic.coffeemachine.evStartCoffee");
            Class.forName("com.telelogic.coffeemachine.evStartTea");
            Class.forName("com.telelogic.coffeemachine.evTea");
    
        // Registering Static Classes 
        
      }
      catch(Exception e) { 
         java.lang.System.err.println(e.toString());
         e.printStackTrace(java.lang.System.err);
      }
    }
    //#]
    
    protected static TheBuilder p_TheBuilder = null;
    private coffeemachine_pkgClass initializer_com_telelogic_coffeemachine = new coffeemachine_pkgClass(RiJMainThread.instance());
    
    //## configuration EXE::Host 
    public static void main(String[] args) {
        RiJOXF.Init(null, 0, 0, true, args);
        MainEXE initializer_EXE = new MainEXE();
        p_TheBuilder = new TheBuilder(RiJMainThread.instance());
        p_TheBuilder.startBehavior();
        //#[ configuration EXE::Host 
        //#]
        RiJOXF.Start();
        p_TheBuilder=null;
    }
    
}
/*********************************************************************
	File Path	: EXE/Host/MainEXE.java
*********************************************************************/

