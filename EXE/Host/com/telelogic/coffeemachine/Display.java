/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: Display
//!	Generated Date	: Fri, 19, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/Display.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/Display.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class Display 
public class Display implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassDisplay = new AnimClass("com.telelogic.coffeemachine.Display",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected Coffeemachine itsCoffeemachine;		//## link itsCoffeemachine 
    
    protected KeyReader itsKeyReader;		//## classInstance itsKeyReader 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int WaitForInput=1;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Display(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassDisplay.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param key
    */
    //## operation processKey(char) 
    public void processKey(char key) {
        try {
            animInstance().notifyMethodEntered("processKey",
               new ArgData[] {
                   new ArgData(char.class, "key", AnimInstance.animToString(key))
               });
        
        //#[ operation processKey(char) 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public Coffeemachine getItsCoffeemachine() {
        return itsCoffeemachine;
    }
    
    //## auto_generated 
    public void __setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        itsCoffeemachine = p_Coffeemachine;
        if(p_Coffeemachine != null)
            {
                animInstance().notifyRelationAdded("itsCoffeemachine", p_Coffeemachine);
            }
        else
            {
                animInstance().notifyRelationCleared("itsCoffeemachine");
            }
    }
    
    //## auto_generated 
    public void _setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        if(itsCoffeemachine != null)
            {
                itsCoffeemachine.__setItsDisplay(null);
            }
        __setItsCoffeemachine(p_Coffeemachine);
    }
    
    //## auto_generated 
    public void setItsCoffeemachine(Coffeemachine p_Coffeemachine) {
        if(p_Coffeemachine != null)
            {
                p_Coffeemachine._setItsDisplay(this);
            }
        _setItsCoffeemachine(p_Coffeemachine);
    }
    
    //## auto_generated 
    public void _clearItsCoffeemachine() {
        animInstance().notifyRelationCleared("itsCoffeemachine");
        itsCoffeemachine = null;
    }
    
    //## auto_generated 
    public KeyReader getItsKeyReader() {
        return itsKeyReader;
    }
    
    //## auto_generated 
    public void __setItsKeyReader(KeyReader p_KeyReader) {
        itsKeyReader = p_KeyReader;
        if(p_KeyReader != null)
            {
                animInstance().notifyRelationAdded("itsKeyReader", p_KeyReader);
            }
        else
            {
                animInstance().notifyRelationCleared("itsKeyReader");
            }
    }
    
    //## auto_generated 
    public void _setItsKeyReader(KeyReader p_KeyReader) {
        if(itsKeyReader != null)
            {
                itsKeyReader.__setItsDisplay(null);
            }
        __setItsKeyReader(p_KeyReader);
    }
    
    //## auto_generated 
    public KeyReader newItsKeyReader(RiJThread p_thread) {
        itsKeyReader = new KeyReader(p_thread);
        itsKeyReader._setItsDisplay(this);
        animInstance().notifyRelationAdded("itsKeyReader", itsKeyReader);
        return itsKeyReader;
    }
    
    //## auto_generated 
    public void deleteItsKeyReader() {
        itsKeyReader.__setItsDisplay(null);
        animInstance().notifyRelationRemoved("itsKeyReader", itsKeyReader);
        itsKeyReader=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsKeyReader = newItsKeyReader(p_thread);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsKeyReader.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            if(rootState_subState == WaitForInput)
                {
                    WaitForInput_add(animStates);
                }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(rootState_active == WaitForInput)
                {
                    res = WaitForInput_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void WaitForInput_add(AnimStates animStates) {
            animStates.add("ROOT.WaitForInput");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void WaitForInput_exit() {
            WaitForInputExit();
            animInstance().notifyStateExited("ROOT.WaitForInput");
        }
        
        //## statechart_method 
        public int WaitForInput_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evKeyPressed.evKeyPressed_coffeemachine_telelogic_com_id))
                {
                    res = WaitForInputTakeevKeyPressed();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void WaitForInput_entDef() {
            WaitForInput_enter();
        }
        
        //## statechart_method 
        public int WaitForInputTakeevKeyPressed() {
            evKeyPressed params = (evKeyPressed) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            //## transition 1 
            if(params.key == 'c')
                {
                    animInstance().notifyTransitionStarted("0");
                    animInstance().notifyTransitionStarted("1");
                    WaitForInput_exit();
                    //#[ transition 1 
                    itsCoffeemachine.gen(new evStartCoffee());
                    //#]
                    WaitForInput_entDef();
                    animInstance().notifyTransitionEnded("1");
                    animInstance().notifyTransitionEnded("0");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            else
                {
                    //## transition 2 
                    if(params.key == 'e')
                        {
                            animInstance().notifyTransitionStarted("0");
                            animInstance().notifyTransitionStarted("2");
                            WaitForInput_exit();
                            //#[ transition 2 
                            itsCoffeemachine.gen(new evEmpty());
                            //#]
                            WaitForInput_entDef();
                            animInstance().notifyTransitionEnded("2");
                            animInstance().notifyTransitionEnded("0");
                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                        }
                    else
                        {
                            //## transition 3 
                            if(params.key == 'f')
                                {
                                    animInstance().notifyTransitionStarted("0");
                                    animInstance().notifyTransitionStarted("3");
                                    WaitForInput_exit();
                                    //#[ transition 3 
                                    itsCoffeemachine.gen(new evRefilled());
                                    //#]
                                    WaitForInput_entDef();
                                    animInstance().notifyTransitionEnded("3");
                                    animInstance().notifyTransitionEnded("0");
                                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                                }
                            else
                                {
                                    //## transition 4 
                                    if(params.key == 'z')
                                        {
                                            animInstance().notifyTransitionStarted("0");
                                            animInstance().notifyTransitionStarted("4");
                                            WaitForInput_exit();
                                            //#[ transition 4 
                                            System.exit(1);
                                            //#]
                                            WaitForInput_entDef();
                                            animInstance().notifyTransitionEnded("4");
                                            animInstance().notifyTransitionEnded("0");
                                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                                        }
                                    else
                                        {
                                            //## transition 6 
                                            if(params.key == 't')
                                                {
                                                    animInstance().notifyTransitionStarted("0");
                                                    animInstance().notifyTransitionStarted("6");
                                                    WaitForInput_exit();
                                                    //#[ transition 6 
                                                    itsCoffeemachine.gen(new evStartTea());
                                                    //#]
                                                    WaitForInput_entDef();
                                                    animInstance().notifyTransitionEnded("6");
                                                    animInstance().notifyTransitionEnded("0");
                                                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                                                }
                                        }
                                }
                        }
                }
            return res;
        }
        
        //## statechart_method 
        public void WaitForInputExit() {
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("5");
            WaitForInput_entDef();
            animInstance().notifyTransitionEnded("5");
        }
        
        //## statechart_method 
        public void WaitForInput_enter() {
            animInstance().notifyStateEntered("ROOT.WaitForInput");
            rootState_subState = WaitForInput;
            rootState_active = WaitForInput;
            WaitForInputEnter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void WaitForInputEnter() {
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Display.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassDisplay; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsKeyReader", true, true, itsKeyReader);
        msg.add("itsCoffeemachine", false, true, itsCoffeemachine);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Display.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Display.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Display.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/Display.java
*********************************************************************/

