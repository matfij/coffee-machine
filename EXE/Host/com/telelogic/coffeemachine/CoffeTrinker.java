/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: CoffeTrinker
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/CoffeTrinker.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/CoffeTrinker.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


/**
[[ * @author $Author]]
[[ * @version $Version]]
[[ * @see $See]]
[[ * @since $Since]]
*/
//## actor CoffeTrinker 
public class CoffeTrinker implements Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassCoffeTrinker = new AnimClass("com.telelogic.coffeemachine.CoffeTrinker",false);
    //#]
    
    
    // Constructors
    
    //## auto_generated 
    public  CoffeTrinker() {
        try {
            animInstance().notifyConstructorEntered(animClassCoffeTrinker.getUserClass(),
               new ArgData[] {
               });
        
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassCoffeTrinker; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(CoffeTrinker.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            CoffeTrinker.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            CoffeTrinker.this.addRelations(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/CoffeTrinker.java
*********************************************************************/

