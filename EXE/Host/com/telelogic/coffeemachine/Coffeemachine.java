/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: Coffeemachine
//!	Generated Date	: Sun, 21, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/Coffeemachine.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/Coffeemachine.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## class Coffeemachine 
public class Coffeemachine implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassCoffeemachine = new AnimClass("com.telelogic.coffeemachine.Coffeemachine",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected int boilTime;		//## attribute boilTime 
    
    protected int grindTime;		//## attribute grindTime 
    
    protected int mixTime;		//## attribute mixTime 
    
    protected Display itsDisplay;		//## link itsDisplay 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int Waiting=1;
    public static final int RefilTea=2;
    public static final int RefilCoffee=3;
    public static final int makingTea=4;
    public static final int makingTea_Mixing=5;
    public static final int makingTea_Boiling=6;
    public static final int makingCoffee=7;
    public static final int Mixing=8;
    public static final int Grinding=9;
    public static final int Boiling=10;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    protected int makingTea_subState;		//## ignore 
    
    protected int makingTea_lastState;		//## ignore 
    
    public static final int Coffeemachine_Timeout_makingTea_Mixing_id = 1;		//## ignore 
    
    public static final int Coffeemachine_Timeout_makingTea_Boiling_id = 2;		//## ignore 
    
    protected int makingCoffee_subState;		//## ignore 
    
    protected int makingCoffee_lastState;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Mixing_id = 3;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Grinding_id = 4;		//## ignore 
    
    public static final int Coffeemachine_Timeout_Boiling_id = 5;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Coffeemachine(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassCoffeemachine.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation setupCoffee() 
    public void setupCoffee() {
        try {
            animInstance().notifyMethodEntered("setupCoffee",
               new ArgData[] {
               });
        
        //#[ operation setupCoffee() 
        boilTime = 2000;
        grindTime = 2000;
        mixTime = 2000;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation setupTea() 
    public void setupTea() {
        try {
            animInstance().notifyMethodEntered("setupTea",
               new ArgData[] {
               });
        
        //#[ operation setupTea() 
        boilTime = 300;
        mixTime = 300;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public int getBoilTime() {
        return boilTime;
    }
    
    //## auto_generated 
    public void setBoilTime(int p_boilTime) {
        boilTime = p_boilTime;
    }
    
    //## auto_generated 
    public int getGrindTime() {
        return grindTime;
    }
    
    //## auto_generated 
    public void setGrindTime(int p_grindTime) {
        grindTime = p_grindTime;
    }
    
    //## auto_generated 
    public int getMixTime() {
        return mixTime;
    }
    
    //## auto_generated 
    public void setMixTime(int p_mixTime) {
        mixTime = p_mixTime;
    }
    
    //## auto_generated 
    public Display getItsDisplay() {
        return itsDisplay;
    }
    
    //## auto_generated 
    public void __setItsDisplay(Display p_Display) {
        itsDisplay = p_Display;
        if(p_Display != null)
            {
                animInstance().notifyRelationAdded("itsDisplay", p_Display);
            }
        else
            {
                animInstance().notifyRelationCleared("itsDisplay");
            }
    }
    
    //## auto_generated 
    public void _setItsDisplay(Display p_Display) {
        if(itsDisplay != null)
            {
                itsDisplay.__setItsCoffeemachine(null);
            }
        __setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void setItsDisplay(Display p_Display) {
        if(p_Display != null)
            {
                p_Display._setItsCoffeemachine(this);
            }
        _setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void _clearItsDisplay() {
        animInstance().notifyRelationCleared("itsDisplay");
        itsDisplay = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(makingCoffee_subState == state)
                {
                    return true;
                }
            if(makingTea_subState == state)
                {
                    return true;
                }
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case makingCoffee:
                {
                    makingCoffee_add(animStates);
                }
                break;
                case Waiting:
                {
                    Waiting_add(animStates);
                }
                break;
                case RefilCoffee:
                {
                    RefilCoffee_add(animStates);
                }
                break;
                case makingTea:
                {
                    makingTea_add(animStates);
                }
                break;
                case RefilTea:
                {
                    RefilTea_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case Grinding:
                {
                    res = Grinding_takeEvent(id);
                }
                break;
                case Boiling:
                {
                    res = Boiling_takeEvent(id);
                }
                break;
                case Mixing:
                {
                    res = Mixing_takeEvent(id);
                }
                break;
                case Waiting:
                {
                    res = Waiting_takeEvent(id);
                }
                break;
                case RefilCoffee:
                {
                    res = RefilCoffee_takeEvent(id);
                }
                break;
                case makingTea_Boiling:
                {
                    res = makingTea_Boiling_takeEvent(id);
                }
                break;
                case makingTea_Mixing:
                {
                    res = makingTea_Mixing_takeEvent(id);
                }
                break;
                case RefilTea:
                {
                    res = RefilTea_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void Waiting_add(AnimStates animStates) {
            animStates.add("ROOT.Waiting");
        }
        
        //## statechart_method 
        public void RefilTea_add(AnimStates animStates) {
            animStates.add("ROOT.RefilTea");
        }
        
        //## statechart_method 
        public void RefilCoffee_add(AnimStates animStates) {
            animStates.add("ROOT.RefilCoffee");
        }
        
        //## statechart_method 
        public void makingTea_add(AnimStates animStates) {
            animStates.add("ROOT.makingTea");
            switch (makingTea_subState) {
                case makingTea_Boiling:
                {
                    makingTea_Boiling_add(animStates);
                }
                break;
                case makingTea_Mixing:
                {
                    makingTea_Mixing_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void makingTea_Mixing_add(AnimStates animStates) {
            animStates.add("ROOT.makingTea.Mixing");
        }
        
        //## statechart_method 
        public void makingTea_Boiling_add(AnimStates animStates) {
            animStates.add("ROOT.makingTea.Boiling");
        }
        
        //## statechart_method 
        public void makingCoffee_add(AnimStates animStates) {
            animStates.add("ROOT.makingCoffee");
            switch (makingCoffee_subState) {
                case Grinding:
                {
                    Grinding_add(animStates);
                }
                break;
                case Boiling:
                {
                    Boiling_add(animStates);
                }
                break;
                case Mixing:
                {
                    Mixing_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void Mixing_add(AnimStates animStates) {
            animStates.add("ROOT.makingCoffee.Mixing");
        }
        
        //## statechart_method 
        public void Grinding_add(AnimStates animStates) {
            animStates.add("ROOT.makingCoffee.Grinding");
        }
        
        //## statechart_method 
        public void Boiling_add(AnimStates animStates) {
            animStates.add("ROOT.makingCoffee.Boiling");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
            makingTea_subState = RiJNonState;
            makingTea_lastState = RiJNonState;
            makingCoffee_subState = RiJNonState;
            makingCoffee_lastState = RiJNonState;
        }
        
        //## statechart_method 
        public void GrindingEnter() {
            //#[ state makingCoffee.Grinding.(Entry) 
            System.out.println("");
            System.out.print(" Grinding beans: "); 
                 
            for(int i = 0; i < 101; i ++)   
            { 
            	if(i%10 == 0)
            	{          
                    System.out.print("|");   
            		try
            		{
            		    Thread.sleep(grindTime/10);
            		}
            		catch(InterruptedException ex)
            		{
            		    Thread.currentThread().interrupt();
            		}
            	}
            }
                            
            //#]
            itsRiJThread.schedTimeout(0, Coffeemachine_Timeout_Grinding_id, this, "ROOT.makingCoffee.Grinding");
        }
        
        //## statechart_method 
        public void makingCoffeeEnter() {
        }
        
        //## statechart_method 
        public void makingCoffee_entDef() {
            makingCoffee_enter();
            
            animInstance().notifyTransitionStarted("2");
            Grinding_entDef();
            animInstance().notifyTransitionEnded("2");
        }
        
        //## statechart_method 
        public int WaitingTakeevStartTea() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("8");
            Waiting_exit();
            //#[ transition 8 
            setupTea();
            //#]
            makingTea_entDef();
            animInstance().notifyTransitionEnded("8");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int MixingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Mixing_id)
                {
                    animInstance().notifyTransitionStarted("3");
                    makingCoffee_exit();
                    Waiting_entDef();
                    animInstance().notifyTransitionEnded("3");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void makingCoffeeExit() {
        }
        
        //## statechart_method 
        public void RefilCoffeeExit() {
            //#[ state RefilCoffee.(Exit) 
                 System.out.println(" beans refilled! \n");
            //#]
        }
        
        //## statechart_method 
        public int Grinding_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = GrindingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = makingCoffee_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void makingTea_entDef() {
            makingTea_enter();
            
            animInstance().notifyTransitionStarted("9");
            makingTea_Boiling_entDef();
            animInstance().notifyTransitionEnded("9");
        }
        
        //## statechart_method 
        public void RefilCoffee_exit() {
            RefilCoffeeExit();
            animInstance().notifyStateExited("ROOT.RefilCoffee");
        }
        
        //## statechart_method 
        public void RefilTea_entDef() {
            RefilTea_enter();
        }
        
        //## statechart_method 
        public void Waiting_enter() {
            animInstance().notifyStateEntered("ROOT.Waiting");
            rootState_subState = Waiting;
            rootState_active = Waiting;
            WaitingEnter();
        }
        
        //## statechart_method 
        public void Grinding_exit() {
            GrindingExit();
            animInstance().notifyStateExited("ROOT.makingCoffee.Grinding");
        }
        
        //## statechart_method 
        public void makingCoffee_exit() {
            makingCoffee_lastState = makingCoffee_subState;
            switch (makingCoffee_subState) {
                case Grinding:
                {
                    Grinding_exit();
                    makingCoffee_lastState = Grinding;
                }
                break;
                case Boiling:
                {
                    Boiling_exit();
                    makingCoffee_lastState = Boiling;
                }
                break;
                case Mixing:
                {
                    Mixing_exit();
                    makingCoffee_lastState = Mixing;
                }
                break;
                default:
                    break;
            }
            makingCoffee_subState = RiJNonState;
            makingCoffeeExit();
            animInstance().notifyStateExited("ROOT.makingCoffee");
        }
        
        //## statechart_method 
        public int makingTea_Boiling_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = makingTea_BoilingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = makingTea_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int makingTea_MixingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_makingTea_Mixing_id)
                {
                    animInstance().notifyTransitionStarted("11");
                    makingTea_exit();
                    Waiting_entDef();
                    animInstance().notifyTransitionEnded("11");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int makingTea_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evEmpty.evEmpty_coffeemachine_telelogic_com_id))
                {
                    res = makingTeaTakeevEmpty();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void makingTeaExit() {
        }
        
        //## statechart_method 
        public int Boiling_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = BoilingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = makingCoffee_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int makingCoffeeTakeevEmpty() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("6");
            makingCoffee_exit();
            RefilCoffee_entDef();
            animInstance().notifyTransitionEnded("6");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int makingTea_Mixing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = makingTea_MixingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = makingTea_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void makingTea_Mixing_enter() {
            animInstance().notifyStateEntered("ROOT.makingTea.Mixing");
            makingTea_subState = makingTea_Mixing;
            rootState_active = makingTea_Mixing;
            makingTea_MixingEnter();
        }
        
        //## statechart_method 
        public int makingTeaTakeevEmpty() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("12");
            makingTea_exit();
            RefilTea_entDef();
            animInstance().notifyTransitionEnded("12");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int RefilCoffeeTakeevRefilled() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            RefilCoffee_exit();
            makingCoffee_enter();
            makingCoffee_entShallowHist();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int RefilTeaTakeevRefilled() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("13");
            RefilTea_exit();
            makingTea_enter();
            makingTea_entShallowHist();
            animInstance().notifyTransitionEnded("13");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Waiting_entDef() {
            Waiting_enter();
        }
        
        //## statechart_method 
        public void Grinding_enter() {
            animInstance().notifyStateEntered("ROOT.makingCoffee.Grinding");
            makingCoffee_subState = Grinding;
            rootState_active = Grinding;
            GrindingEnter();
        }
        
        //## statechart_method 
        public int makingTea_BoilingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_makingTea_Boiling_id)
                {
                    animInstance().notifyTransitionStarted("10");
                    makingTea_Boiling_exit();
                    makingTea_Mixing_entDef();
                    animInstance().notifyTransitionEnded("10");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void makingTea_BoilingEnter() {
            //#[ state makingTea.Boiling.(Entry) 
            System.out.println("");
            System.out.print(" Boiling water: "); 
                 
            for(int i = 0; i < 101; i ++)   
            { 
            	if(i%10 == 0)
            	{          
                    System.out.print("|");   
            		try
            		{
            		    Thread.sleep(boilTime/10);
            		}
            		catch(InterruptedException ex)
            		{
            		    Thread.currentThread().interrupt();
            		}
            	}
            }
            //#]
            itsRiJThread.schedTimeout(0, Coffeemachine_Timeout_makingTea_Boiling_id, this, "ROOT.makingTea.Boiling");
        }
        
        //## statechart_method 
        public void RefilTeaEnter() {
            //#[ state RefilTea.(Entry) 
              System.out.println("\n need more leafes! ");     
            
            //#]
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void Mixing_entDef() {
            Mixing_enter();
        }
        
        //## statechart_method 
        public void makingCoffee_entShallowHist() {
            if(makingCoffee_lastState != RiJNonState)
                {
                    makingCoffee_subState = makingCoffee_lastState;
                    switch (makingCoffee_subState) {
                        case Grinding:
                        {
                            Grinding_enter();
                        }
                        break;
                        case Boiling:
                        {
                            Boiling_enter();
                        }
                        break;
                        case Mixing:
                        {
                            Mixing_enter();
                        }
                        break;
                        default:
                            break;
                    }
                }
        }
        
        //## statechart_method 
        public void makingTea_Mixing_entDef() {
            makingTea_Mixing_enter();
        }
        
        //## statechart_method 
        public void RefilTeaExit() {
            //#[ state RefilTea.(Exit) 
                 System.out.println(" leafs refilled! \n");
            //#]
        }
        
        //## statechart_method 
        public void RefilTea_enter() {
            animInstance().notifyStateEntered("ROOT.RefilTea");
            rootState_subState = RefilTea;
            rootState_active = RefilTea;
            RefilTeaEnter();
        }
        
        //## statechart_method 
        public void makingTea_enter() {
            animInstance().notifyStateEntered("ROOT.makingTea");
            rootState_subState = makingTea;
            makingTeaEnter();
        }
        
        //## statechart_method 
        public void RefilCoffee_enter() {
            animInstance().notifyStateEntered("ROOT.RefilCoffee");
            rootState_subState = RefilCoffee;
            rootState_active = RefilCoffee;
            RefilCoffeeEnter();
        }
        
        //## statechart_method 
        public void WaitingEnter() {
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public int BoilingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Boiling_id)
                {
                    animInstance().notifyTransitionStarted("1");
                    Boiling_exit();
                    Mixing_entDef();
                    animInstance().notifyTransitionEnded("1");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void Mixing_enter() {
            animInstance().notifyStateEntered("ROOT.makingCoffee.Mixing");
            makingCoffee_subState = Mixing;
            rootState_active = Mixing;
            MixingEnter();
        }
        
        //## statechart_method 
        public void MixingEnter() {
            //#[ state makingCoffee.Mixing.(Entry) 
            System.out.println("");
            System.out.print("  Mixing coffee: "); 
                 
            for(int i = 0; i < 101; i ++)   
            { 
            	if(i%10 == 0)
            	{          
                    System.out.print("|");   
            		try
            		{
            		    Thread.sleep(mixTime/10);
            		}
            		catch(InterruptedException ex)
            		{
            		    Thread.currentThread().interrupt();
            		}
            	}
            }
            //#]
            itsRiJThread.schedTimeout(0, Coffeemachine_Timeout_Mixing_id, this, "ROOT.makingCoffee.Mixing");
        }
        
        //## statechart_method 
        public void makingTea_Boiling_exit() {
            makingTea_BoilingExit();
            animInstance().notifyStateExited("ROOT.makingTea.Boiling");
        }
        
        //## statechart_method 
        public void makingTea_MixingEnter() {
            //#[ state makingTea.Mixing.(Entry) 
            System.out.println("");
            System.out.print("  Mixing tea: "); 
                 
            for(int i = 0; i < 101; i ++)   
            { 
            	if(i%10 == 0)
            	{          
                    System.out.print("|");   
            		try
            		{
            		    Thread.sleep(mixTime/10);
            		}
            		catch(InterruptedException ex)
            		{
            		    Thread.currentThread().interrupt();
            		}
            	}
            }
            //#]
            itsRiJThread.schedTimeout(0, Coffeemachine_Timeout_makingTea_Mixing_id, this, "ROOT.makingTea.Mixing");
        }
        
        //## statechart_method 
        public void makingTeaEnter() {
        }
        
        //## statechart_method 
        public void RefilTea_exit() {
            RefilTeaExit();
            animInstance().notifyStateExited("ROOT.RefilTea");
        }
        
        //## statechart_method 
        public int Waiting_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evStartCoffee.evStartCoffee_coffeemachine_telelogic_com_id))
                {
                    res = WaitingTakeevStartCoffee();
                }
            else if(event.isTypeOf(evStartTea.evStartTea_coffeemachine_telelogic_com_id))
                {
                    res = WaitingTakeevStartTea();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void BoilingEnter() {
            //#[ state makingCoffee.Boiling.(Entry) 
            System.out.println("");
            System.out.print("  Boiling Water: "); 
                 
            for(int i = 0; i < 101; i ++)   
            { 
            	if(i%10 == 0)
            	{          
                    System.out.print("|");   
            		try
            		{
            		    Thread.sleep(boilTime/10);
            		}
            		catch(InterruptedException ex)
            		{
            		    Thread.currentThread().interrupt();
            		}
            	}
            }
            //#]
            itsRiJThread.schedTimeout(0, Coffeemachine_Timeout_Boiling_id, this, "ROOT.makingCoffee.Boiling");
        }
        
        //## statechart_method 
        public int Mixing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = MixingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = makingCoffee_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void makingTea_entShallowHist() {
            if(makingTea_lastState != RiJNonState)
                {
                    makingTea_subState = makingTea_lastState;
                    switch (makingTea_subState) {
                        case makingTea_Boiling:
                        {
                            makingTea_Boiling_enter();
                        }
                        break;
                        case makingTea_Mixing:
                        {
                            makingTea_Mixing_enter();
                        }
                        break;
                        default:
                            break;
                    }
                }
        }
        
        //## statechart_method 
        public void Boiling_entDef() {
            Boiling_enter();
        }
        
        //## statechart_method 
        public void MixingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Mixing_id, this);
            //#[ state makingCoffee.Mixing.(Exit) 
            for(int i = 1; i < 99; i++){
            System.out.println("");
            }
            
            System.out.println("            COFFEE & TEA MACHINE        ");
            System.out.println(" designed and developed by Mateusz Fijak");   
            System.out.println("");
            
            System.out.println(" ****************************************");
            System.out.println(" *     c - make a coffee                *");                     
            System.out.println(" *     t - make a tea                   *");  
            System.out.println(" *     e - take out beans/leafes        *");
            System.out.println(" *     f - refill beans/leafes          *");   
            System.out.println(" *     z - shut down the machine        *");     
            System.out.println(" ****************************************");
            
            System.out.println("");
            System.out.println(" Your coffee is ready!");    
            System.out.println("");
            System.out.println("");    
            
            System.out.println("\007");
            //#]
        }
        
        //## statechart_method 
        public void makingCoffee_enter() {
            animInstance().notifyStateEntered("ROOT.makingCoffee");
            rootState_subState = makingCoffee;
            makingCoffeeEnter();
        }
        
        //## statechart_method 
        public int RefilCoffee_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evRefilled.evRefilled_coffeemachine_telelogic_com_id))
                {
                    res = RefilCoffeeTakeevRefilled();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int WaitingTakeevStartCoffee() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("14");
            Waiting_exit();
            //#[ transition 14 
            setupCoffee();
            //#]
            makingCoffee_entDef();
            animInstance().notifyTransitionEnded("14");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Waiting_exit() {
            WaitingExit();
            animInstance().notifyStateExited("ROOT.Waiting");
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("7");
            Waiting_entDef();
            animInstance().notifyTransitionEnded("7");
        }
        
        //## statechart_method 
        public void Boiling_enter() {
            animInstance().notifyStateEntered("ROOT.makingCoffee.Boiling");
            makingCoffee_subState = Boiling;
            rootState_active = Boiling;
            BoilingEnter();
        }
        
        //## statechart_method 
        public void Mixing_exit() {
            MixingExit();
            animInstance().notifyStateExited("ROOT.makingCoffee.Mixing");
        }
        
        //## statechart_method 
        public void makingTea_Boiling_enter() {
            animInstance().notifyStateEntered("ROOT.makingTea.Boiling");
            makingTea_subState = makingTea_Boiling;
            rootState_active = makingTea_Boiling;
            makingTea_BoilingEnter();
        }
        
        //## statechart_method 
        public void makingTea_Mixing_exit() {
            makingTea_MixingExit();
            animInstance().notifyStateExited("ROOT.makingTea.Mixing");
        }
        
        //## statechart_method 
        public void makingTea_exit() {
            makingTea_lastState = makingTea_subState;
            switch (makingTea_subState) {
                case makingTea_Boiling:
                {
                    makingTea_Boiling_exit();
                    makingTea_lastState = makingTea_Boiling;
                }
                break;
                case makingTea_Mixing:
                {
                    makingTea_Mixing_exit();
                    makingTea_lastState = makingTea_Mixing;
                }
                break;
                default:
                    break;
            }
            makingTea_subState = RiJNonState;
            makingTeaExit();
            animInstance().notifyStateExited("ROOT.makingTea");
        }
        
        //## statechart_method 
        public void Boiling_exit() {
            BoilingExit();
            animInstance().notifyStateExited("ROOT.makingCoffee.Boiling");
        }
        
        //## statechart_method 
        public void GrindingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Grinding_id, this);
        }
        
        //## statechart_method 
        public void Grinding_entDef() {
            Grinding_enter();
        }
        
        //## statechart_method 
        public void makingTea_Boiling_entDef() {
            makingTea_Boiling_enter();
        }
        
        //## statechart_method 
        public void RefilCoffee_entDef() {
            RefilCoffee_enter();
        }
        
        //## statechart_method 
        public int GrindingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Coffeemachine_Timeout_Grinding_id)
                {
                    animInstance().notifyTransitionStarted("0");
                    Grinding_exit();
                    Boiling_entDef();
                    animInstance().notifyTransitionEnded("0");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void WaitingExit() {
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void BoilingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_Boiling_id, this);
        }
        
        //## statechart_method 
        public int makingCoffee_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evEmpty.evEmpty_coffeemachine_telelogic_com_id))
                {
                    res = makingCoffeeTakeevEmpty();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void makingTea_BoilingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_makingTea_Boiling_id, this);
        }
        
        //## statechart_method 
        public void makingTea_MixingExit() {
            itsRiJThread.unschedTimeout(Coffeemachine_Timeout_makingTea_Mixing_id, this);
            //#[ state makingTea.Mixing.(Exit) 
            for(int i = 1; i < 99; i++){
            System.out.println("");
            }
            
            System.out.println("            COFFEE & TEA MACHINE        ");
            System.out.println(" designed and developed by Mateusz Fijak");   
            System.out.println("");
            
            System.out.println(" ****************************************");
            System.out.println(" *     c - make a coffee                *");                     
            System.out.println(" *     t - make a tea                   *");  
            System.out.println(" *     e - take out beans/leafes        *");
            System.out.println(" *     f - refill beans/leafes          *");   
            System.out.println(" *     z - shut down the machine        *");     
            System.out.println(" ****************************************");
            
            System.out.println("");
            System.out.println(" Your tea is ready!");    
            System.out.println("");
            System.out.println("");  
            
            System.out.println("\007");  
            
            //#]
        }
        
        //## statechart_method 
        public void RefilCoffeeEnter() {
            //#[ state RefilCoffee.(Entry) 
              System.out.println("\n need more beans!");   
            
            //#]
        }
        
        //## statechart_method 
        public int RefilTea_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evRefilled.evRefilled_coffeemachine_telelogic_com_id))
                {
                    res = RefilTeaTakeevRefilled();
                }
            
            return res;
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Coffeemachine.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassCoffeemachine; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("grindTime", grindTime);
        msg.add("boilTime", boilTime);
        msg.add("mixTime", mixTime);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsDisplay", false, true, itsDisplay);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Coffeemachine.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Coffeemachine.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Coffeemachine.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/Coffeemachine.java
*********************************************************************/

