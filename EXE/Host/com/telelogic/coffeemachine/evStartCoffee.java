/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: evStartCoffee
//!	Generated Date	: Fri, 19, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/evStartCoffee.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evStartCoffee.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evStartCoffee() 
public class evStartCoffee extends RiJEvent implements AnimatedEvent {
    
    public static final int evStartCoffee_coffeemachine_telelogic_com_id = 29219;		//## ignore 
    
    
    // Constructors
    
    public  evStartCoffee() {
        lId = evStartCoffee_coffeemachine_telelogic_com_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evStartCoffee_coffeemachine_telelogic_com_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("com.telelogic.coffeemachine.evStartCoffee");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evStartCoffee(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/evStartCoffee.java
*********************************************************************/

