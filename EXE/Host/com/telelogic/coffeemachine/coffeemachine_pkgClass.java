/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: com.telelogic.coffeemachine
//!	Generated Date	: Mon, 8, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/coffeemachine_pkgClass.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.telelogic.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxfinst.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/coffeemachine_pkgClass.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## ignore 
public class coffeemachine_pkgClass {
    
    public static object_0 object_0;		//## classInstance object_0 
    
    
    // Constructors
    
    //## auto_generated 
    public  coffeemachine_pkgClass(RiJThread p_thread) {
        initRelations();
    }
    
    //## auto_generated 
    protected void finalize() throws Throwable {
        
        super.finalize();
    }
    
    private static void renameGlobalInstances() {
        if(object_0 != null)
            {
                AnimServices.setInstanceName(object_0, "object_0");
            }
    }
    
    //## auto_generated 
    protected void initRelations() {
        object_0 = new object_0();
        renameGlobalInstances();
    }
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/coffeemachine_pkgClass.java
*********************************************************************/

