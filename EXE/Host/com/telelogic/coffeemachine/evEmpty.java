/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: evEmpty
//!	Generated Date	: Fri, 19, Apr 2019 
	File Path	: EXE/Host/com/telelogic/coffeemachine/evEmpty.java
*********************************************************************/

package com.telelogic.coffeemachine;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// com/telelogic/coffeemachine/evEmpty.java                                                                  
//----------------------------------------------------------------------------

//## package com::telelogic::coffeemachine 


//## event evEmpty() 
public class evEmpty extends RiJEvent implements AnimatedEvent {
    
    public static final int evEmpty_coffeemachine_telelogic_com_id = 29217;		//## ignore 
    
    
    // Constructors
    
    public  evEmpty() {
        lId = evEmpty_coffeemachine_telelogic_com_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evEmpty_coffeemachine_telelogic_com_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("com.telelogic.coffeemachine.evEmpty");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evEmpty(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/com/telelogic/coffeemachine/evEmpty.java
*********************************************************************/

