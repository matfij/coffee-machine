echo off

set RHAP_JARS_DIR=C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib
set FRAMEWORK_NONE_JARS=C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\oxf.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\anim.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\animcom.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\oxfInstMock.jar;
set FRAMEWORK_ANIM_JARS=C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\oxf.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\anim.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\animcom.jar;C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share\LangJava\lib\oxfInst.jar;
set SOURCEPATH=%SOURCEPATH%
set CLASSPATH=%CLASSPATH%;.
set PATH=%RHAP_JARS_DIR%;%PATH%;
set INSTRUMENTATION=Animation   

set BUILDSET=Debug

if %INSTRUMENTATION%==Animation goto anim

:noanim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_NONE_JARS%
goto setEnv_end

:anim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_ANIM_JARS%

:setEnv_end

if "%1" == "" goto compile
if "%1" == "build" goto compile
if "%1" == "clean" goto clean
if "%1" == "rebuild" goto clean
if "%1" == "run" goto run

:clean
echo cleaning class files
if exist com\telelogic\coffeemachine\coffeemachine_pkgClass.class del com\telelogic\coffeemachine\coffeemachine_pkgClass.class
if exist com\telelogic\coffeemachine\TheBuilder.class del com\telelogic\coffeemachine\TheBuilder.class
if exist com\telelogic\coffeemachine\evRefilled.class del com\telelogic\coffeemachine\evRefilled.class
if exist com\telelogic\coffeemachine\evEmpty.class del com\telelogic\coffeemachine\evEmpty.class
if exist com\telelogic\coffeemachine\evKeyPressed.class del com\telelogic\coffeemachine\evKeyPressed.class
if exist com\telelogic\coffeemachine\evStartCoffee.class del com\telelogic\coffeemachine\evStartCoffee.class
if exist com\telelogic\coffeemachine\evStartTea.class del com\telelogic\coffeemachine\evStartTea.class
if exist com\telelogic\coffeemachine\TeaTrinker.class del com\telelogic\coffeemachine\TeaTrinker.class
if exist MainEXE.class del MainEXE.class
if exist com\telelogic\coffeemachine\evTea.class del com\telelogic\coffeemachine\evTea.class
if exist com\telelogic\coffeemachine\KeyReader.class del com\telelogic\coffeemachine\KeyReader.class
if exist com\telelogic\coffeemachine\ServicePerson.class del com\telelogic\coffeemachine\ServicePerson.class
if exist com\telelogic\coffeemachine\Display.class del com\telelogic\coffeemachine\Display.class
if exist com\telelogic\coffeemachine\CoffeTrinker.class del com\telelogic\coffeemachine\CoffeTrinker.class
if exist com\telelogic\coffeemachine\Coffeemachine.class del com\telelogic\coffeemachine\Coffeemachine.class
if exist com\telelogic\coffeemachine\object_0.class del com\telelogic\coffeemachine\object_0.class

if "%1" == "clean" goto end

:compile   
if %BUILDSET%==Debug goto compile_debug
echo compiling JAVA source files
javac  @files.lst
goto end

:compile_debug
echo compiling JAVA source files
javac -g  @files.lst
goto end

:run

java %2

:end


